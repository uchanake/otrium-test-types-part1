# API Tests

## Run Performance tests

**Please follow the instruction to execute the Performance tests**
1. Download and open the Jmeter and import the .jmx project
2. Feed the data file location __uuidData.csv__
3. Set thread count and run the tests