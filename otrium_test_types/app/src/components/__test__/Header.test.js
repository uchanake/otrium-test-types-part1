import React from "react";
import ReactDOM from "react-dom";
import Header from '../Header';

import {render, cleanup} from '@testing-library/react';
import "@testing-library/jest-dom/extend-expect";

import renderer from 'react-test-renderer';

afterEach(cleanup);

it("rederes without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(<Header></Header>, div);
});

it("validate header text", () => {
    const { getByRole } = render(<Header></Header>);
    const headerValue = getByRole('heading');
    expect(headerValue).toHaveTextContent('Contact Manager');
});

it("match snapshot header", ()=>{
    const tree = renderer.create(<Header></Header>).toJSON();
    expect(tree).toMatchSnapshot();
});