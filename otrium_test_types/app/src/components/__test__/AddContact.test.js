import React from "react";
import ReactDOM from "react-dom";
import AddContact from '../AddContact';

import { render, cleanup, fireEvent } from '@testing-library/react';
import "@testing-library/jest-dom/extend-expect";

import renderer from 'react-test-renderer';

const setup = (locator) => {
    const utils = render(<AddContact />)
    const input = utils.getByTestId(locator);
    return {
        input,
        ...utils,
    }
}

afterEach(cleanup);

it("rederes without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(<AddContact></AddContact>, div);
});

it("validate header text", () => {
    const { getByRole } = render(<AddContact></AddContact>);
    const headerValue = getByRole('heading');
    expect(headerValue).toHaveTextContent('Add Contact');
});

it("validate button text", () => {
    const { getByTestId } = render(<AddContact></AddContact>);
    const buttonText = getByTestId('submit button');
    expect(buttonText).toHaveTextContent("Add");
});

it("validate text filed count", () => {
    const { getAllByRole } = render(<AddContact></AddContact>);
    const placeHolderText = getAllByRole('textbox');
    expect(placeHolderText).toHaveLength(2);
});

it('validate the input in Name field', () => {
    const { input } = setup('name');
    fireEvent.change(input, { target: { value: 'jack' } });
    expect(input.value).toBe('jack');
});

it('validate the input in Email field', () => {
    const { input } = setup('email');
    fireEvent.change(input, { target: { value: 'jack@gmail.com' } });
    expect(input.value).toBe('jack@gmail.com');
});

it("match snapshot Add Contact", () => {
    const tree = renderer.create(<AddContact></AddContact>).toJSON();
    expect(tree).toMatchSnapshot();
});

describe("validate add contact button", () => {
    describe("with default values", () => {
        it("does return error message", () => {
            const add = jest.fn();
            const alertMock = jest.spyOn(window, 'alert').mockImplementation();

            const { queryByTestId } = render(<AddContact>onSubmit={add}</AddContact>);
            const button = queryByTestId('submit button');
            fireEvent.click(button);

            expect(alertMock).toHaveBeenCalledTimes(1);
        });
    });

    describe("with test values", () => {
        it("does not return error message", () => {
            const add = jest.fn();
            const alertMock = jest.spyOn(window, 'alert').mockImplementation();

            const { queryByTestId } = render(<AddContact>onSubmit={add}</AddContact>);
            fireEvent.change(queryByTestId('name'), { target: { value: 'jj' } });
            fireEvent.change(queryByTestId('email'), { target: { value: 'jj@gmail.com' } });
            const button = queryByTestId('submit button');
            fireEvent.click(button);
            expect(alertMock).toHaveBeenCalledTimes(0);
        });
    });
});