# Unit tests, Component tests & Visual Tests

## Run Unit tests & Component tests

**Please follow the instruction to execute the unit tests & componenet tests**
1. Open the project otrium_test_types in VSCode
2. In the console go inside the server folder and run below commands to install npm dependencies and spin up the server
```
npm install
npm run start
```
3. In another console go inside the app folder and run below commands to install npm dependencies and launch the sample app
```
npm install
npm run start
```
4. In another console go inside the app folder and run below command to run unit & component tests
```
npm run test
```

## Run Visual tests
**Please follow the instruction to execute the visual tests**
1. In console go inside the visualtests folder and run below command to run visual tests
```
npm install
npm run test
```

P.S: Make sure the server and up and running to execute the visual tests