const HashMap = require('hashmap');
const hashMap = new HashMap();
let EC = protractor.ExpectedConditions;

const ELEMENT_TIME_OUT = 60000;
const LOADING_TIME_OUT = 60000;

const PerformOnScreen = function () {

    /**
     * Load the aut application
     * @param {string} url 
     */
    this.Open = function (url) {
        browser.ignoreSynchronization = true;
        browser.get(url);
        browser.waitForAngularEnabled(false);
    }
    
    /**
     * Switch to alert box
     * @returns Alert box focus
     */
    this.SwitchToAlertBox = function () {
        browser.wait(EC.alertIsPresent(), LOADING_TIME_OUT);
        const alertBox = browser.switchTo().alert();
        return alertBox;
    }

    /**
     * Dismiss alert box
     * @param {object} alertObject 
     */
    this.DismissAlertBox = function (alertObject) {
        alertObject.dismiss();
    }

    /**
     * Wait for UIElement display
     * @param {oject} uiElement 
     */
    this.WaitForUIElementDisplay = function (uiElement) {
        browser.wait(EC.presenceOf(uiElement), ELEMENT_TIME_OUT, "Time Out Error: Element not precence of " + ELEMENT_TIME_OUT + " milliseconds");
    }

    /**
     * Type on element
     * @param {object} uielement 
     * @param {string} texttotype 
     */
    this.Type = function (uielement, texttotype) {
        browser.wait(EC.presenceOf(uielement), ELEMENT_TIME_OUT, "Time Out Error " + ELEMENT_TIME_OUT + " milliseconds");
        if ((texttotype == null) || (texttotype == '')) {
            uielement.isDisplayed().then(function (isVisible) {
                expect(isVisible).toBe(true);
                uielement.clear();
                console.log("Cleared the text");
            });
        } else {
            uielement.isDisplayed().then(function (isVisible) {
                expect(isVisible).toBe(true);
                uielement.clear();
                uielement.sendKeys(texttotype);
                console.log("Typed On Element");
                browser.wait(EC.presenceOf(uielement), ELEMENT_TIME_OUT, "Time Out Error: Element does not presence after " + ELEMENT_TIME_OUT + " milliseconds");
                expect(uielement.getAttribute('value')).toBe('' + texttotype, "Error..! : Type on element not performed");
            });
        }
    }

    /**
     * Click on element
     * @param {object} uiElement 
     */
    this.Click = function (uielement) {
        browser.wait(EC.presenceOf(uielement), ELEMENT_TIME_OUT, "Time Out Error: Element does not presence after " + ELEMENT_TIME_OUT + " milliseconds");
        browser.wait(EC.elementToBeClickable(uielement), ELEMENT_TIME_OUT, "Time Out Error: Element cannot clickable after " + ELEMENT_TIME_OUT + " milliseconds");
        uielement.click();
    }
}

module.exports = new PerformOnScreen();