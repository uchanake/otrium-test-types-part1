const { browser } = require('protractor');
var using = require('jasmine-data-provider');
const AddContactPage = require('../pom/AddContactPage');
const ContactListPage = require('../pom/ContactListPage');
const GLOBAL_DATA = require('../testdata/globalData.json');
const POSITIVE_TEST_DATA = require('../testdata/addContactTestData.json');
const NEGATIVE_TEST_DATA = require('../testdata/negativeTestData.json');

describe('verify the user adding functionality', function () {

    beforeEach(function () {
        originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 450000;

        ContactListPage.NavigateToApp(GLOBAL_DATA.baseURL);
    });

    using(POSITIVE_TEST_DATA, function (data) {
        it("add new user to the contact list", function () {
            ContactListPage.NaviateToAddContanct();
            AddContactPage.FillContactDetails(data.name, data.email);
            ContactListPage.VerifyAddedContact(data.name, data.email);
        });
    });

    using(NEGATIVE_TEST_DATA, function (data) {
        it("add new user to the contact list", function () {
            ContactListPage.NaviateToAddContanct();
            AddContactPage.FillContactDetails(data.name, data.email);
            AddContactPage.VerifyAlertBox(data.message);
        });
    });
});