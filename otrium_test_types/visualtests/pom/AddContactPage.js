const { element, browser } = require('protractor');
const BasePage = require('../utils/BasePage');

const txtName = element(by.xpath("//input[@name='name']"));
const txtEmail = element(by.xpath("//input[@name='email']"));
const btnAdd = element(by.buttonText('Add'));

const AddContact = function () {

    this.FillContactDetails = function (name, email) {
        actionEnterName(name);
        actionEnterEmail(email);
        actionClickAdd();
    }

    this.VerifyAlertBox = function (message) {
        const alertDialog = BasePage.SwitchToAlertBox();
        expect(alertDialog.getText()).toEqual(message);
        BasePage.DismissAlertBox(alertDialog);
    }

    const actionEnterName = function (name) {
        BasePage.Type(txtName, name);
    }

    const actionEnterEmail = function (email) {
        BasePage.Type(txtEmail, email);
    }

    const actionClickAdd = function () {
        BasePage.Click(btnAdd);
    }

}
module.exports = new AddContact();