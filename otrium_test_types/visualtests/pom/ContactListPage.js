const { element } = require('protractor');
const BasePage = require('../utils/BasePage');

const txtHeader = element(by.tagName('h2'));
const btnAddContact = element(by.buttonText('Add Contact'));
const txtNameList = element.all(by.css('a > .header'));
const txtEmail = function (email) {
    return element(by.xpath("//div[text()='" + email + "']/following-sibling::div"));
}

const ContactListPage = function () {

    this.NavigateToApp = function (baseURL) {
        BasePage.Open(baseURL);
        BasePage.WaitForUIElementDisplay(txtHeader);
    }

    this.NaviateToAddContanct = function () {
        BasePage.Click(btnAddContact);
    }

    this.VerifyAddedContact = function (name, email) {
        BasePage.WaitForUIElementDisplay(txtHeader);
        verifyNameAddedLast(name);
        verifyEmailOfTheName(name, email);
    }

    this.VerifyAddedContact = function (name, email) {
        BasePage.WaitForUIElementDisplay(txtHeader);
        verifyNameAddedLast(name);
        verifyEmailOfTheName(name, email);
    }

    const verifyNameAddedLast = function (name) {
        expect(txtNameList.last().getText()).toBe(name, "Error...! : Last name not matched.");
    }

    const verifyEmailOfTheName = function (name, email) {
        expect(txtEmail(name).getText()).toBe(email, "Error...! : Email not matched with the given name.")
    }
}
module.exports = new ContactListPage();