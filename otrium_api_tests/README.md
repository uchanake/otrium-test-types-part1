# API Tests

## Run API tests

**Please follow the instruction to execute the API tests**
1. Open the Postman and import the project
2. Select the Collection and click run
3. Browse the data file __collectionUserDataFile.csv__ and click to start the tests